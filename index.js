const express = require('express');
const objectsController = require('./modulos/objects/controlador');
const { connectToServer } = require("./mongodb");
const {logger} = require('./logger')


const port = process.env.OBJECTS_PORT

async function runServer(){
  process.on('uncaughtException', err => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on('unhandledRejection', (err, err2) => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on("SIGTERM", (err) => {
    logger.error({action: "Stopping...", data: {error: err}});
    setTimeout(() => { mprocess.exit(1);}, 100).unref();
  });

  try {
    connectToServer();
    logger.info({ event:"MongoDB loaded and connected" });
  } catch (error) {
    logger.error({ event: "MongoDB ERROR: ", error });
    process.exit(1);
  }

  const app = express()

  // Parser de body y cookies
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.get("/test", (req, res) => {
    res.header("Content-Type", "application/json");
    res.writeHead(200);
    res.end("Todo ok");
  });

  app.post('/objects/create', (req, res) => { 
    objectsController.create(req, res);
  })

  app.post('/objects/objects', (req, res) => { 
    objectsController.getAll(req, res);
  })

  app.post('/objects/object-types', (req, res) => { 
    objectsController.getTypes(req, res);
  })

  app.post('/objects/search', (req, res) => { 
    objectsController.search(req, res);
  })

  app.post('/objects/recent', (req, res) => { 
    objectsController.searchRecent(req, res);
  })

  app.get('/', (req, res) => {
    res.writeHead(200, 'Content-Type', 'application/json'); 
    res.end('Servicio corriendo ok');
  })

  app.listen(port, function () { logger.info({ action: 'Service running on port ' + port}) } )
}

runServer();








