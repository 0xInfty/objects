const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const object = req.body
        logger.info({ action: 'Request to create object' })
        if (!object.magic) {object.magicRarity="Ninguna"}
        entryId = await service.createObject(object)
        logger.info({ action: 'Object created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(entryId))
    } catch {
        logger.info({ action: 'Fail en object creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en object creation"}))
    }
}

async function getAll(req, res){

    logger.info({ action: 'request to get objects'})

    const objects = await service.getObjects()
    console.log("controller: " + objects)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objects))
}

async function getTypes(req, res){

    logger.info({ action: 'request to get object types'})

    const objectTypeObjects = await service.getObjectTypes()
    var objectTypes = []
    for (var piece of objectTypeObjects) {
      objectTypes.push( piece['name'] )
    }
    console.log("controller: " + objectTypes)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectTypes))
}

async function search(req, res){

    logger.debug({ action: 'request to search object'})
    const searchString = req.body.searchString

    const objectsFound = await service.search(searchString)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function searchRecent(req, res){

    logger.debug({ action: 'request to search recent objects'})
    const maxNumber = req.body.maxNumber

    const objectsFound = await service.searchRecent(maxNumber)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getAll, getTypes, search, searchRecent }