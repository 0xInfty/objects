const { ObjectID } = require("bson");
const { getDb } = require("../../mongodb");

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function createObjectType( objectType ) { return new Promise(async (resolve, reject) => { 

    const objectsTypesDocuments = await getDb().collection('object_types').findOne({name: objectType})

    if(!objectsTypesDocuments) {
        getDb().collection('object_types').insertOne({name: objectType}).then( () => {   
            consoleLogDebug(`Object Type successfully added: ${objectType}`, 1)
            resolve ({'resultObjectType':'success' })
        }).catch( error => {
            consoleLogDebug(`Object Type not successfully added: ${objectType}`)
            consoleLogDebug(error)
            reject(error)
        })   
    } else{
        resolve ({'resultObjectType':'success' })
    }
})}

async function createObject(object){ return new Promise(async (resolve, reject) => {
    
    object.created_ts = Date.now()
    const entryId = ObjectID()
    object._id = entryId
    const entryIdString = ObjectID(entryId).toString()
    consoleLogDebug(entryId, 1)
    
    await getDb().collection('objects').insertOne(object).catch( error => {
        console.error(error);
        reject(error);
    })

    const objectType = object.objectType;
    await createObjectType(objectType).catch( error => {
        console.error(error);
        reject(error);
    })

    consoleLogDebug(entryIdString, 1)
    resolve( entryIdString )
})}

async function getObjects(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all objects", 1);
    getDb().collection('objects').find().toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "objects" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function getObjectTypes(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all object types", 1);
    getDb().collection('object_types').find().toArray(function (err, docs) {
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function search( searchString ){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to search objects, string: " + searchString, 1);
    getDb().collection('objects').find({ $text: {$search: searchString}},{ score:{ $meta: "textScore"}}).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "objects" }
        resolve (docs, 2);
    })
})}

async function searchRecent( maxNumber ) { return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search the most recent ${maxNumber} objects`, 1);
    getDb().collection('objects').find().sort({"created_ts": -1}).limit(maxNumber).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "objects" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

module.exports = { createObject, getObjects, getObjectTypes, search, searchRecent }